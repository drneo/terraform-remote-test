variable "kafka_cluster_id" {
  description = "The ID of the Kafka cluster."
  type        = string
}

variable "kafka_rest_endpoint" {
  description = "kafka_rest_endpoint"
  type = string
}

variable "kafka_api_key" {
  description = "kafka_api_key "
  type = string
}

variable "kafka_api_secret" {
  description = "kafka_api_secret"
  type = string
}
 
variable "cloud_api_key" {
  type = string
}

variable "cloud_api_secret" {
  type = string
}


variable "service" {
  type = string
}

variable "environment" {
  type = string
}

variable "kafka_cluster_env_id" {
  type = string
}