provider "confluent" {
  kafka_id            = var.kafka_cluster_id                   # optionally use KAFKA_ID env var
  kafka_rest_endpoint = var.kafka_rest_endpoint        # optionally use KAFKA_REST_ENDPOINT env var
  kafka_api_key       = var.kafka_api_key              # optionally use KAFKA_API_KEY env var
  kafka_api_secret    = var.kafka_api_secret           # optionally use KAFKA_API_SECRET env var
  cloud_api_key = var.cloud_api_key 
  cloud_api_secret = var.cloud_api_secret
}

resource "confluent_kafka_topic" "topic" {
  kafka_cluster {
    id = var.kafka_cluster_id
  }
  rest_endpoint      = var.kafka_rest_endpoint

  topic_name  = "event.${var.service}-${var.environment}.log"
  config = {
    "cleanup.policy"  = "delete"
    "retention.bytes" = "1209600000"
  }
}

resource "confluent_service_account" "sa" {
  display_name = "${var.service}-${var.environment}"
  description  = "self-service account for ${var.service}-${var.environment}"
}

resource "confluent_role_binding" "rb" {
  principal   = "User:${confluent_service_account.sa.id}"
  role_name   = "DeveloperWrite"
  crn_pattern = "crn://confluent.cloud/organization=3e85ac3a-af6a-4b51-835e-68c50f35b733/environment=${var.kafka_cluster_env_id}/cloud-cluster=${var.kafka_cluster_id}/kafka=${var.kafka_cluster_id}/topic=${confluent_kafka_topic.topic.topic_name}"
}
resource "confluent_api_key" "apikey" {
  display_name = "${var.service}-${var.environment}"
  description  = "self service ${var.service}-${var.environment}"
  owner {
    id          = confluent_service_account.sa.id
    api_version = "iam/v2"
    kind        = "ServiceAccount"
  }

  managed_resource {
    id          = var.kafka_cluster_id
    api_version = "cmk/v2"
    kind        = "Cluster"

    environment {
      id = var.kafka_cluster_env_id
    }
  }
}
output "api_key_id" {
  value = confluent_api_key.apikey.id
  sensitive = true
}

output "api_key_secret" {
  value     = confluent_api_key.apikey.secret
  sensitive = true
}
